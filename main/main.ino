#include "DHT.h"

#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)

#define DATA_SIZE 50

//#define DHTTYPE DHT10

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT dht(DHTPIN, DHTTYPE);

#if defined(ARDUINO_ARCH_AVR)
#define debug  Serial

#elif defined(ARDUINO_ARCH_SAMD) ||  defined(ARDUINO_ARCH_SAM)
#define debug  SerialUSB
#else
#define debug  Serial
#endif

/*
 * w : waterRemaining &
 * t : temperature &
 * h : humidity &
*/

/*
  Data : 3 bytes de 256 bits
    - byte 1 : waterRemaining (1 false, 2 true)
    - byte 2 : humudity ( 1 to 100 , 127 for error)
    - byte 3 : temperature : (1 to 100 , 125 to 127 for error)
      - divisé par 2 : 1 - > 0.5°C, 100 -> 50°C
      - 125 : < 0°C, 126 : > 50°C 127 : generic error
*/

int pin = 9;
int duration = 0;
int index = 0;

void setup() {
  Serial.begin(9600);
//  Wire.begin();
//  dht.begin();
}

//void loop() {
////  Serial.print(getHumidity());
////  Serial.print(" ");
////  Serial.println(getTemp());
//  delay(1500);
//}

void loop() {
  if(index == 5){
    turnOnPin(pin, duration);
    index = 0;
    printData();
  }
  if (Serial.available() > 0) {
    int incomingByte = Serial.read();

    if(index == 3){
      pin = incomingByte;
    }else if(index == 4){
      float floatDuration = (float) incomingByte;
      duration = floatDuration/10.0;
    }
  
//    Serial.print(index);
//    Serial.print(" : ");
//    Serial.println(incomingByte, DEC);
    index++;
  }
}

void printData(){
  int waterRemaining = 2;
  int temp = (int) roundf(getTemperature()*2);
  int humidity = (int) roundf(readHumidity());
  Serial.print((char) waterRemaining);
  Serial.print((char) temp);
  Serial.print((char) humidity);
  Serial.print((char) pin);
  Serial.print((char) 101);
}

float readHumidity() {
  float temp_hum_val[2] = {0};
  if (!dht.readTempAndHumidity(temp_hum_val)) {
    return temp_hum_val[0];
  } else {
    return 127.0;
  }
}

float getTemperature() {
  float temp_hum_val[2] = {0};
  if (!dht.readTempAndHumidity(temp_hum_val)) {
    return temp_hum_val[1];
  } else {
    return 127.0;
  }
}

void turnOnPin(int pin, double duration) {
  // Serial.print("e");
  digitalWrite(pin, HIGH);
  delay(duration * 1000);
  digitalWrite(pin, LOW);
}
