# Entrées/Sorties #
5 bits qui sont lus puis écrits sur le port série :
1. waterRemaining (1 false 2 true)
2. humidity ( 1 à 100 , 127 pour les erreurs)
3. temperature : (1 à 100 , 125 à 127 pour les erreurs)
 * divisé par 2 : 1 - > 0.5°C, 100 -> 50°C
 * erreurs : 125 : <= 0°C, 126 : > 50°C 127 : erreur de lecture
4. pin à activer
5. durée de l'activation du pin, retourne 101 après avoir arrosé en cas de succès